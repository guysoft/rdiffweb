# Rdiffweb configuration

Since v2.2.0, Rdiffweb configuration is more flexible. You may configure it using
configuration file, environment variables and command line arguments.

All args that start with '--' (e.g.: --debug) can also be set in configuration file or environment variables.
By default, Rdiffweb loads configuration files from `/etc/rdiffweb/rdw.conf` or `/etc/rdiffweb/rdw.conf.d/*.conf`.
Environment variable are prefix with `RDIFFWEB_` where dash (-) should be replace by underscore (_). e.g. `--server-host 8081` could be defined using `RDIFFWEB_SERVER_HOST=8081`.

Configuration file syntax allows: 

    # This is a comments
    key=value
    flag=true
    
If a value is specified in more than one place, then commandline values
override environment variables which override config file values which override defaults.

## Database

Rdiffweb use SQL database to store user preferences. The embedded SQLite database is well suited for small deployment (1-100 users). If you intended to have a large deployment, you must also consider using a PostgreSQL database instead.

| Parameter | Description | Required | Example |
| --- | --- | --- | --- |
| --database-uri | Location of the database used for persistence. SQLite and PostgreSQL database are supported officially. To use a SQLite database you may define the location using a file path or a URI. e.g.: /srv/rdiffweb/file.db or sqlite:///srv/rdiffweb/file.db`. To use PostgreSQL server you must provide a URI similar to postgresql://user:pass@10.255.1.34/dbname and you must install required dependencies. By default, Rdiffweb uses a SQLite embedded database located at /etc/rdiffweb/rdw.db. | No | postgresql://user:pass@10.255.1.34/dbname | 


###SQLite

To use embedded SQLite database, pass the config `--database-uri` with a URI similar to `sqlite:///etc/rdiffweb/rdw.db` or `/etc/rdiffweb/rdw.db`.

###PostgreSQL

To use an external PostgreSQL database, pass the config `--database-uri` with a URI similar to `postgresql://user:pass@10.255.1.34/dbname`.

You may need to install additional dependencies to connect to PostgreSQL. Step to install dependencies might differ according to the way you installed Rdiffweb.

**Using pip (from Pypi):**

    pip install psycopg2-binary

**Using apt for Debian:**

    apt install python3-psycopg2

## LDAP Authentication

Rdiffweb integrates with LDAP to support user authentication. 

This integration works with most LDAP-compliant directory servers, including:

* Microsoft Active Directory
* Apple Open Directory
* Open LDAP
* 389 Server

**Requirements**

Make sure you have `python-ldap` installed either from your Linux distribution repository or from pypi using `pip`.


**LDAP parameters**

| Parameter | Description | Required | Example |
| --- | --- | --- | --- |
| --ldap-uri | URIs containing only the schema, the host, and the port. | Yes | ldap://localhost:389 | 
| --ldap-tls | `true` to enable TLS. Default to `false` | No | false |
| --ldap-protocol-version | Version of LDAP in use either 2 or 3. Default to 3. | No | 3 |
| --ldap-base-dn | The DN of the branch of the directory where all searches should start from. | Yes | dc=my,dc=domain | 
| --ldap-bind-dn | An optional DN used to bind to the server when searching for entries. If not provided, will use an anonymous bind. | No | cn=manager,dc=my,dc=domain |
| --ldap-bind-password |  A bind password to use in conjunction with `LdapBindDn`. Note that the bind password is probably sensitive data, and should be properly protected. You should only use the LdapBindDn and LdapBindPassword if you absolutely need them to search the directory. | No | mypassword |
| --ldap-user-attribute | The attribute to search username. If no attributes are provided, the default is to use `uid`. It's a good idea to choose an attribute that will be unique across all entries in the subtree you will be using. | No | cn | 
| --ldap-scope | The scope of the search. Can be either `base`, `onelevel` or `subtree`. Default to `subtree`. | No | onelevel |
| --ldap-filter | A valid LDAP search filter. If not provided, defaults to `(objectClass=*)`, which will search for all objects in the tree. | No | (objectClass=*) | 
| --ldap-network-timeout | Optional timeout value. Default to 10 sec. | No | 10 |
| --ldap-timeout | Optional timeout value. Default to 300 sec. | No | 300 |
| --ldap-allow-password-change | `true` to allow LDAP users to  update their password using rdiffweb. This option should only be enabled if the LDAP if confiugred to allow the user to change their own password. Default to  `false`. | No | true |
| --ldap-add-missing-user | `True` to create users from LDAP when the credential are valid. | No | True |
| --ldap-add-user-default-role | Role to be used when creating a new user from LDAP. Default: user | No | maintainer |
| --ldap-add-user-default-userroot | Userroot to be used when creating a new user from LDAP. Default: empty | No | /backups/{cn[0]} |

## Email notifications

Since Rdiffweb v0.9, you may setup notification when your backup did not
run for a given period of time. This is useful to know when your backup
setup is not working. This section describes how to configure rdiffweb to notify you.

**Edit config file**

Edit rdiffweb config file `/etc/rdiffweb/rdw.conf` and edit the `Email*`
configuration parameters to suit your environment. The following is an example
using a gmail account to sent notification.

    #----- Enable Email Notification
    # The server can be configured to email user when their repositories have not
    # been backed up for a user-specified period of time. To enable this feature,
    # set below settings to correct values.
    EmailNotificationTime=6:30
    EmailHost=smtp.gmail.com:587
    EmailEncryption=starttls
    EmailSender=example@gmail.com
    EmailUsername=example@gmail.com
    EmailPassword=CHANGEME

**Restart rdiffweb**

To apply the modification, restart rdiffweb as follow:

    sudo service rdiffweb restart

**Edit your preferences**

Login to rdiffweb using your username & password. Browse to your user settings.
A new tab named "Notification" you be displayed. Click on it to change the
notification settings for each repository.

## User's quota management

Since 2.1.0, it's now possible to customize how user quota is controller for
your system without a custom plugin. By defining `QuotaSetCmd`, `QuotaGetCmd`
and `QuotaUsedCmd` configuration options, you have all the flexibility to
manage the quota the way you want by providing custom command line to be
executed to respectively set the quota, get the quota and get quota usage.

When calling the script, special environment variables are available:

* RDIFFWEB_USERID: rdiffweb user id. e.g.: `34`
* RDIFFWEB_USERNAME: rdiffweb user name. e.g.: `patrik`
* RDIFFWEB_USERROOT: user's root directory. e.g.: `/backups/patrik/`
* RDIFFWEB_ROLE: user's role e.g.: `10` 1:Admin, 5:Maintainer, 10:User
* RDIFFWEB_QUOTA: only available for `QuotaSetCmd`. Define the new quota value in bytes. e.g.: 549755813888  (0.5 TiB)


| Parameter | Description | Required | 
| --- | --- | --- |
| --quota-set-cmd | Command line to set the user's quota. | Yes. If you want to allow administrators to set quota from the web interface. |
| --quota-get-cmd | Command line to get the user's quota. Should print the size in bytes to console. | No. Default behaviour gets quota using operating system statvfs that should be good if you are using setquota, getquota, etc. For ZFS and other more exotic file system, you may need to define this command. |
| --quota-used-cmd | Command line to get the quota usage. Should print the size in bytes to console. | No. |

Continue reading about how to configure quotas for EXT4. We generally
recommend making use of project quotas with rdiffweb to simplify the management
of permission and avoid running rdiffweb with root privileges.  The next section
present how to configure project quota. Keep in mind it's also possible to
configure quota using either user's quota or project quota.

### Configure user quota for ext4

This section is not a full documentation about how to configure ext4 project quota
but provide enough guidance to help you.

1. Enabled project quota feature  
   You must enable project quota feature for the ext4 partition where your backup resides using:  
   `tune2fs -O project -Q prjquota /dev/sdaX`  
   The file system must be unmounted to change this setting and may require you
   to boot you system with a live-cd if your backups reside on root file system (`/`).  
   Also, add `prjquota` options to your mount point configuration `/etc/fstab`.
   Something like `/dev/sdaX   /   ext4    errors=remount-ro,prjquota     0    1`
2. Turn on the project quota after reboot  
   `quotaon -Pv -F vfsv1 /`
3. Check if the quota is working  
   `repquota -Ps /`
4. Add `+P` attribute on directory you which to control quota  
   `chattr -R +P /backups/admin`
5. Then set the project id on directories  
   `chattr -R -p 1 /backups/admin` where `1` is the rdiffweb user's id

Next you may configure rdiffweb quota command line for your need. For ext4
project quotas, you only need to define `QuotaSetCmd` with something similar to:

    QuotaSetCmd=setquota -P $RDIFFWEB_USERID $((RDIFFWEB_QUOTA / 1024)) $((RDIFFWEB_QUOTA / 1024)) 0 0 /

This effectively, makes use of rdiffweb user's id as project id.

### Configure user quota for zfs

This section is not a full documentation about how to configure ZFS project quotas
but provide enough guidance to help you. This documentation uses `tank/backups`
as the dataset to store rdiffweb backups.

1. Quota feature is a relatively new feature for ZFS On Linux. Check your
   operating system to verify if your ZFS version support it. You may need
   to upgrade your pool and dataset using:  
   
   `zpool upgrade tank`
   `zfs upgrade tank/backups`
   
2. Add `+P` attribute on directory you which to control quota  
   `chattr -R +P /backups/admin`
   `chattr -R -p 1 /backups/admin`
   OR
   `zfs project -p 1 -rs /backups/admin`
   Where `1` is the rdiffweb user's id
   
Take note, it's better to enable project quota attributes when the repositories are empty.

## SSH Key management

Rdiffweb allow users to manage their SSH Keys by adding and removing them using the web interface.
This feature may be disable with `--disable-ssh-keys`.

When this feature is enabled, adding or removing an SSH Key from the web interface
updates the `${user_root}/.ssh/authorized_keys` file if the file already exists.

## Other Rdiffweb settings

| Parameter | Description | Required | Example |
| --- | --- | --- | --- |
| --server-host | Define the IP address to listen to. Use 0.0.0.0 to listen on all interfaces. | No | 127.0.0.1 |
| --server-port | Define the host to listen to. Default to 8080 | No | 80 |
| --log-level | Define the log level. ERROR, WARN, INFO, DEBUG | No | DEBUG |
| --environment | Define the type of environment: development, production. This is used to limit the information shown to the user when an error occur. | No | production |
| --header-name | Define the application name displayed in the title bar and header menu. | No | My Backup |
| --default-theme | Define the default theme. Either: default or orange. Define the css file to be loaded in the web interface. You may manually edit a css file to customize it. the location is similar to `/usr/local/lib/python2.7/dist-packages/rdiffweb/static/`. It's preferable to contact the developer if you want a specific color scheme to be added. | No | orange |
| --welcome-msg | Replace the headling displayed in the login page | No | - |
| --log-file | Define the location of the log file | No | /var/log/rdiffweb.log |
| --log-access-file | Define the location of the access log file | No | /var/log/rdiffweb-access.log |
| --remove-older-time | Time when to execute the remove older task | No | 22:00 | 
| --sqlitedb-file | Location of the SQLite database | No | /etc/rdiffweb/rdw.db | 
| --admin-user | Define the name of the default admin user to be created | No | admin |
| --favicon | Define the FavIcon to be displayed in the browser title | No | /etc/rdiffweb/my-fav.ico |
| --tempdir | Define an alternate temp directory to be used when restoring files. | No | /retore/ |
| --max-depth | Define the maximum folder depthness to search into the user's root directory to find repositories. This is commonly used if you repositories are organised with multiple sub-folder. Default: 5 | No | 10 |
